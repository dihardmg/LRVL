@extends('layout.v_template')
@section('title','Halaman Pendapatan')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header table-responsive">
                        <h3 class="box-title">List Of Pendapatan</h3><br>
                        <a href="" class="btn btn-primary ">Add Data</a>
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>ID Penpdatan</th>
                                    <th>ID Ladang</th>
                                    <th>Nama</th>
                                    <th>Amount</th>
                                    <th>Create Date</th>
                                    <th>Update Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pendapatan as $pendapatans)
                                    <tr>
                                        <td></td>
                                        <td>{{$pendapatans->id_pendapatan}}</td>
                                        <td>{{$pendapatans->id_ladang}}</td>
                                        <td>{{$pendapatans->nama}}</td>
                                        <td>{{$pendapatans->amount}}</td>
                                        <td>{{$pendapatans->create_date}}</td>
                                        <td>{{$pendapatans->update_date}}</td>
                                        <td>
                                            <a href="" class="btn glyphicon glyphicon-list-alt btn-success ">Detail</a>
                                            <a href="" class="btn glyphicon glyphicon-check btn-warning ">Edit</a>
                                            <a href="" class="btn glyphicon glyphicon-trash btn-danger ">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section>
@endsection

