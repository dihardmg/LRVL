<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LadangModel extends Model
{
   public function allDataLadang()
   {
       return DB::table('ladang')->get();
   }
}
