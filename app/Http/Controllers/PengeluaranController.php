<?php

namespace App\Http\Controllers;

use App\Models\PengeluaranModel;
use Illuminate\Http\Request;


class PengeluaranController extends Controller
{
    public function __construct()
    {
        $this->PengeluaranModel = new PengeluaranModel();
    }
    public function index()
    {
        $dataPengeluaran = [
            'pengeluaran'=> $this->PengeluaranModel->allDataPengeluaran()
        ];
        return view('content.v_pengeluaran', $dataPengeluaran);
    }
}
