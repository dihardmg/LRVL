<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LadangModel;
class LadangController extends Controller
{
    public function __construct()
    {
        $this->LadangModel = new LadangModel();
    }

    public function index()
    {
        $dataLadang = [
            'ladang'=> $this->LadangModel->allDataLadang()
            ];
        return view('content.v_ladang',$dataLadang);
    }
}
