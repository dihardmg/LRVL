<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PendapatanModel;

class PendapatanController extends Controller
{
    public function __construct()
    {
        $this->PendapatanModel = new PendapatanModel();
    }
    public function index()
    {
        $dataPendapatan = [
            'pendapatan'=> $this->PendapatanModel->allDataPendapatan()
        ];
        return view('content.v_pendapatan', $dataPendapatan);
    }
}
