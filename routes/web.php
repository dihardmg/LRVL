<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\LadangController;
use App\Http\Controllers\PendapatanController;
use App\Http\Controllers\PengeluaranController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'index']);
Route::get('/ladang',[LadangController::class,'index']);
Route::get('/pendapatan',[PendapatanController::class,'index']);
Route::get('/pengeluaran',[PengeluaranController::class,'index']);
