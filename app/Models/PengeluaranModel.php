<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PengeluaranModel extends Model
{
    public function allDataPengeluaran()
    {
        return DB::table('pengeluaran')->get();
    }
}
