<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li class="{{request()-> is('/') ? 'active' : ''}}"><a href="/"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-th"></i> <span>Master Menu</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="{{request()-> is('ladang') ? 'active' : ''}}"><a href="/ladang"><i class="fa fa-circle-o"></i> Master Ladang</a></li>
            <li class="{{request()-> is('pendapatan') ? 'active' : ''}}"><a href="/pendapatan"><i class="fa fa-circle-o"></i> Master Pendapatan</a></li>
            <li class="{{request()-> is('pengeluaran') ? 'active' : ''}}"><a href="/pengeluaran"><i class="fa fa-circle-o"></i> Master Pengeluaran</a></li>
            </li>

        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-share"></i> <span>Report Revenue</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Total Revenue</a></li>
            </li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-share"></i> <span>Report Expense</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Total Expense</a></li>
            </li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-pie-chart"></i> <span>Report Income Statement</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Total Income</a></li>
            </li>
        </ul>
    </li>
</ul>
